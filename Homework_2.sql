-- 1. Для поточної максимальної річної заробітної плати в компанії ПОКАЗАТИ ПІБ працівника, департамент, 
-- поточну посаду, тривалість перебування на поточній посаді та загальний стаж роботи в компанії.

SELECT CONCAT(e.first_name, ' ', e.last_name) AS employee, 
		d.dept_name, 
        t.title,
		TIMESTAMPDIFF(YEAR, t.from_date, CURDATE()) AS years_cur_position,  
		TIMESTAMPDIFF(YEAR, e.hire_date, CURDATE()) AS work_exp
FROM employees AS e
JOIN dept_emp AS de ON e.emp_no = de.emp_no
JOIN departments AS d ON de.dept_no = d.dept_no
JOIN titles AS t ON de.emp_no = t.emp_no
JOIN salaries AS s ON t.emp_no = s.emp_no
WHERE s.to_date > NOW() AND
      t.to_date > NOW() AND 
      s.salary = (SELECT MAX(salary) FROM employees.salaries AS salary);

-- 2. Для кожного департамента покажіть його назву, ім’я та прізвище поточного керівника та його поточну зарплату.
SELECT d.dept_no, d.dept_name, CONCAT(e.first_name, ' ', e.last_name) AS manager, s.salary 
FROM dept_manager AS dm
JOIN departments AS d ON d.dept_no = dm.dept_no
JOIN employees AS e ON dm.emp_no = e.emp_no
JOIN salaries AS s ON e.emp_no = s.emp_no AND s.to_date = '9999-01-01'
WHERE dm.to_date = '9999-01-01';

-- 3. Покажіть для кожного працівника їхню поточну зарплату та поточну зарплату поточного керівника
SELECT de.emp_no AS employee, s_emp.salary AS emp_salary,
       dm.emp_no AS manager, s_man.salary AS man_salary
FROM dept_emp AS de
JOIN dept_manager AS dm ON dm.dept_no = de.dept_no 
JOIN salaries AS s_emp ON s_emp.emp_no = de.emp_no AND (CURDATE() BETWEEN s_emp.from_date AND s_emp.to_date)
JOIN salaries AS s_man ON s_man.emp_no = dm.emp_no AND (CURDATE() BETWEEN s_man.from_date AND s_man.to_date)
WHERE de.to_date = '9999-01-01' AND dm.to_date = '9999-01-01'
ORDER BY de.emp_no;

-- 4. Покажіть всіх співробітників, які зараз заробляють більше, ніж їхні керівники
SELECT e.emp_no, e.first_name, e.last_name
FROM employees AS e
JOIN salaries AS s ON e.emp_no = s.emp_no AND (CURDATE() BETWEEN s.from_date AND s.to_date)
JOIN dept_emp AS de ON s.emp_no = de.emp_no AND CURDATE() BETWEEN de.from_date AND de.to_date
JOIN dept_manager AS dm ON de.dept_no = dm.dept_no AND CURDATE() BETWEEN dm.from_date AND dm.to_date
JOIN salaries AS sm ON dm.emp_no = sm.emp_no AND (CURDATE() BETWEEN sm.from_date AND sm.to_date) 
WHERE s.salary > sm.salary;

-- 5. Покажіть, скільки співробітників зараз мають кожну посаду. 
-- Відсортуйте в порядку спадання за кількістю співробітників.
SELECT t.title, COUNT(de.emp_no) AS emp_number
FROM titles AS t
JOIN dept_emp AS de ON t.emp_no = de.emp_no AND de.to_date = '9999-01-01'
GROUP BY t.title;

-- 6. Покажіть повні імена всіх співробітників, які працювали більш ніж в одному відділі.
SELECT de.emp_no, CONCAT(e.first_name, ' ', e.last_name) AS full_name
FROM dept_emp AS de
JOIN employees AS e ON de.emp_no = e.emp_no
GROUP BY de.emp_no, e.first_name, e.last_name
HAVING COUNT(de.dept_no) > 1
ORDER BY de.emp_no;

-- 7. Покажіть середню та максимальну зарплату в тисячах доларів за кожен рік
SELECT EXTRACT(YEAR FROM s.from_date) AS year,
	   ROUND(AVG(s.salary) / 1000, 2) AS avg_salary, 
	   ROUND(MAX(s.salary) / 1000, 2) AS max_salary
FROM salaries as s
GROUP BY year
ORDER BY year;

-- 8. Покажіть, скільки працівників було найнято у вихідні дні (субота + неділя), розділивши за статтю
SELECT e.gender AS gender, COUNT(e.emp_no) AS hired_weekends
FROM employees AS e
JOIN (SELECT emp_no, DAYOFWEEK(hire_date) AS chosen_days
	  FROM employees) AS hire_days ON e.emp_no = hire_days.emp_no
WHERE hire_days.chosen_days IN (1, 7)
GROUP BY e.gender;



